<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClinica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinica', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cnpj', 14)->unique();
            $table->string('nome_fantasia');
            $table->string('razao_social')->unique();
            $table->integer('inscricao_estadual')->nullable();
            $table->string('email');
            $table->integer('numero');
            $table->string('telefone', 11);
            $table->string('logradouro');
            $table->string('cep', 8);
            $table->integer('uf_id');
            $table->string('cidade');
            $table->string('bairro');
            $table->string('complemento');
            $table->string('telefone_secundario', 11)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinica');
    }
}
