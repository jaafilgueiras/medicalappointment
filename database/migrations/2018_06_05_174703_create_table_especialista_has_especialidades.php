<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEspecialistaHasEspecialidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialista_has_especialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('especialista_id');
            $table->integer('especialidade_id');
            $table->timestamps();

            $table->foreign('especialista_id')->references('id')->on('especialista')->onDelete('cascade');
            $table->foreign('especialidade_id')->references('id')->on('especialidade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especialista_has_especialidades');
    }
}
