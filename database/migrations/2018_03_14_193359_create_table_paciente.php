<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaciente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('cpf', 11);
            $table->date('data_nascimento');
            $table->string('nome_acompanhante');
            $table->integer('numero');
            $table->string('telefone', 11);
            $table->string('logradouro');
            $table->string('cep', 8);
            $table->integer('uf_id');
            $table->string('cidade');
            $table->string('bairro');
            $table->string('complemento');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente');
    }
}
