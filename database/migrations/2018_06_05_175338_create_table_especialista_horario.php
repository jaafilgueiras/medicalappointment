<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEspecialistaHorario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialista_horario', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_consulta');
            $table->integer('especialista_id');
            $table->timestamps();

            $table->foreign('especialista_id')->references('id')->on('especialista')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especialista_horario');
    }
}
