<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSintomaHasConsequenciaHasSintomas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sintoma_consequencia_sintoma', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patologia_id');
            $table->integer('consequencia_id');
            $table->integer('sintoma_id');
            $table->timestamps();

            $table->foreign('patologia_id')->references('id')->on('patologia')->onDelete('cascade');
            $table->foreign('consequencia_id')->references('id')->on('consequencia')->onDelete('cascade');
            $table->foreign('sintoma_id')->references('id')->on('sintoma')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sintoma_consequencia_sintoma');
    }
}
