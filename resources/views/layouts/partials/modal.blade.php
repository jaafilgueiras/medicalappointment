<div class="modal fade" id="{{ $idModal ? $idModal : 'modal-form-medical' }}" tabindex="-1" role="dialog" aria-labelledby="myModalForm">
    <div class="modal-dialog {{ isset($typeModal) ? $typeModal : 'modal-md' }}" role="document">
        <div id="{{ $idContent ? $idContent : 'content-modal-medical' }}" class="modal-content">
            
        </div>
    </div>
</div>