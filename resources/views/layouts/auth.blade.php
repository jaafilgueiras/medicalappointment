<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    <!-- Bottstrap -->
    <link rel="stylesheet" href="{{ asset('vendor/AdminLTE/bootstrap/css/bootstrap.min.css') }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <style>
        html, body {
            font-family: 'Raleway', sans-serif;
            background: #efefef;
        }
        div.flex-align {
            position: absolute;
            top: 20%; 
            width: 100%; 
            left: 0;
        }
        .info {
            margin-top: 10%;
            text-align: center;
        }
        .form {
            background: #FFFFFF;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
            padding: 20px;
        }
        .form .preview{
            text-align: center;
        }
        .form img{
            width: 150px;
            height:150px;
            margin-bottom: 5%;
        }

        .form input {
            outline: 0;
            background: #f2f2f2;
            color: #000;
            border: 0;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
            box-sizing: border-box;
            font-size: 18px;
        }
        .btn{
            width: 100%;
        }

        #reset-password{
            color: #969696;
            font-weight: bolder;

        }
        
        #reset-password:hover{
            color: #000;
            text-decoration: none;
        }

        .col-md-offset-4-5{margin-left:37.499996666667%}

        @media (max-width: 768px) {
            .col-md-offset-4-5{margin-left:0%}
        }
    </style>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Jquery -->
    <script src=" {{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src=" {{ asset('vendor/AdminLTE/bootstrap/js/bootstrap.min.js') }}"></script>
</head>
<body>
    @yield('content')
</body>
</html>