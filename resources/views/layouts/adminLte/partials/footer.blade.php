<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Version <b>{{ config('app.version') }}</b>
    </div>
    <!-- Default to the left -->
    <strong>Copyright © <?php echo date("Y"); ?> <a href="#">{{ config('app.name') }}</a>.</strong> All rights reserved.
</footer>