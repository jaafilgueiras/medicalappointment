<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('vendor/AdminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="{{ Auth::user()->name }}">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">Dashboard</li>
            <li>
                <a href="{{ url('/panel') }}">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/panel/patologia') }}">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i>
                    <span>Patologias</span>
                </a>
            </li>
            <li class="header">Settings</li>
            <li>
                <a href="{{ url('/panel/sistema') }}">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span>Entidades de Configurações</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/panel/configuracao') }}">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span>Configuração</span>
                </a>
            </li>
        </ul>
    </section>
</aside>