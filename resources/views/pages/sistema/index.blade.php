@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center"> <i class="fa fa-table" aria-hidden="true"></i> Entidades de Configurações</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/clinica') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-hospital-o"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Clinica</span>
                        <span class="info-box-number">{!! $clinica !!}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/consequencia') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-medkit"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Consequência</span>
                        <span class="info-box-number">{!! $consequencia !!}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/funcao') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-gavel"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Funções</span>
                        <span class="info-box-number">{!! $funcoes !!}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/permissao') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-unlock-alt"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Permissões</span>
                        <span class="info-box-number">{!! $permissoes !!}</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/sintoma') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-medkit"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Sintomas</span>
                        <span class="info-box-number">{!! $sintoma !!}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/usuario') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Usuários</span>
                        <span class="info-box-number">{!! $usuarios !!}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/especialidade') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-user-md"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Especialidade</span>
                        <span class="info-box-number">{!! $especialidade !!}</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/tipo-consulta') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-table"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Tipo de Consulta</span>
                        <span class="info-box-number">{!! $tipoConsulta !!}</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/panel/paciente') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-wheelchair"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Paciente</span>
                        <span class="info-box-number">{!! $paciente !!}</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
               

@stop

@section('script-footer')

@stop