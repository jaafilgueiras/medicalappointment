<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
    <h4 class="modal-title text-center"> <i class="fa fa-thermometer-half" aria-hidden="true"></i> {{ (isset($tipoConsulta)) ? 'Editar' : 'Adicionar' }} Tipo de Consulta</h4>
</div>
@if(isset($tipoConsulta))
    {!! Form::model($tipoConsulta, ['action' => ('TipoConsultaController@store'), 'method' => 'post', 'id' => 'form-tipo-consulta']) !!}
@else
    {!! Form::open(['action' => ('TipoConsultaController@store'), 'method' => 'post' , 'id' => 'form-tipo-consulta']) !!}
@endif

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="notify-alert"></div>
        </div>
    </div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id']) !!}
        <div class="col-md-12">
            {!! Form::label('descricao', 'Descrição:') !!}
            {!! Form::text('descricao', null, ['class' => 'form-control', 'id' => 'descricao']) !!}
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default btn-flat" style="float: left !important;" id="fechar-tipo-consulta"> <i class="fa fa-ban" aria-hidden="true"></i></button>
    <button type="submit" class="btn btn-success btn-flat" id="salvar-tipo-consulta"> <i class="fa fa-check" aria-hidden="true"></i></button>
</div>

{!! Form::close() !!}