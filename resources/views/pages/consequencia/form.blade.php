<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
    <h4 class="modal-title text-center"> <i class="fa fa-medkit" aria-hidden="true"></i> {{ (isset($consequencia)) ? 'Editar' : 'Adicionar' }} Consequência</h4>
</div>
@if(isset($consequencia))
    {!! Form::model($consequencia, ['action' => ('ConsequenciaController@store'), 'method' => 'post', 'id' => 'form-consequencia']) !!}
@else
    {!! Form::open(['action' => ('ConsequenciaController@store'), 'method' => 'post' , 'id' => 'form-consequencia']) !!}
@endif

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="notify-alert"></div>
        </div>
    </div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id']) !!}
        <div class="col-md-12">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', null, ['class' => 'form-control', 'id' => 'nome']) !!}
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12">
            {!! Form::label('descricao', 'Descrição:') !!}
            {!! Form::text('descricao', null, ['class' => 'form-control', 'id' => 'descricao']) !!}
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default btn-flat" style="float: left !important;" id="fechar-consequencia"> <i class="fa fa-ban" aria-hidden="true"></i></button>
    <button type="submit" class="btn btn-success btn-flat" id="salvar-consequencia"> <i class="fa fa-check" aria-hidden="true"></i></button>
</div>

{!! Form::close() !!}