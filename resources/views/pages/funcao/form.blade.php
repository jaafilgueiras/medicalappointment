<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
    <h4 class="modal-title text-center"> <i class="fa fa-gavel" aria-hidden="true"></i> {{ (isset($funcao)) ? 'Editar' : 'Adicionar' }} Função</h4>
</div>
@if(isset($funcao))
    {!! Form::model($funcao, ['action' => ('FuncaoController@store'), 'method' => 'post', 'id' => 'form-funcao']) !!}
@else
    {!! Form::open(['action' => ('FuncaoController@store'), 'method' => 'post' , 'id' => 'form-funcao']) !!}
@endif

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="notify-alert"></div>
        </div>
    </div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id']) !!}
        {!! Form::hidden('permissoes', null, ['id' => 'permissions-values']) !!}
        <div class="col-md-12">
            {!! Form::label('name', 'Nome:') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12">
            {!! Form::label('description', 'Descrição:') !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12">
            {!! Form::label('permission_id', 'Permissão :') !!}
            <div class="input-group">
                {!! Form::select('permission_id', $permissions , null, ['class' => 'form-control', 'id' => 'select_permission']) !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat" id="add-permissao"><i class="fa fa-plus"></i></button>
                </span>
            </div>
        </div>
        <div class="col-md-12 table-max-height">
            <br />
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                    </tr>
                </thead>
                <tbody id="table-body-permissoes"></tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default btn-flat" style="float: left !important;" id="fechar-funcao"> <i class="fa fa-ban" aria-hidden="true"></i></button>
    <button type="submit" class="btn btn-success btn-flat" id="salvar-funcao"> <i class="fa fa-check" aria-hidden="true"></i></button>
</div>

{!! Form::close() !!}
<script type="text/javascript">
    loadPermissions();
</script>