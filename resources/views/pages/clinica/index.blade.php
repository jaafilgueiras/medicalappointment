@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center"> <i class="fa fa-hospital-o" aria-hidden="true"></i> Clínica</h3>
                </div>
            </div>
        </div>
    </div>

    @if(isset($clinica))
        {!! Form::model($clinica, ['action' => ('ClinicaController@store'), 'method' => 'post', 'id' => 'form-clinica', 'files' => true]) !!}
    @else
        {!! Form::open(['action' => ('ClinicaController@store'), 'method' => 'post' , 'id' => 'form-clinica', 'files' => true]) !!}
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notify"></div>
                        </div>
                    </div>
                    <fieldset>
                        <legend>Clínica</legend>
                           <br />
                           <div class="row">
                                {!! Form::hidden('id', null, ['id' => 'id']) !!}
                                <div class="col-md-3">
                                    {!! Form::label('cnpj', 'CNPJ:') !!}
                                    {!! Form::text('cnpj', null, ['class' => 'form-control', 'id' => 'cnpj']) !!}
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('nome_fantasia', 'Nome Fantasia:') !!}
                                    {!! Form::text('nome_fantasia', null, ['class' => 'form-control', 'id' => 'nome-fantasia']) !!}
                                </div>
                                <div class="col-md-5">
                                    {!! Form::label('razao_social', 'Razão Social:') !!}
                                    {!! Form::text('razao_social', null, ['class' => 'form-control', 'id' => 'razao-social']) !!}
                                </div>
                           </div>
                           <br />
                           <div class="row">
                                <div class="col-md-3">
                                    {!! Form::label('inscricao_estadual', 'Inscrição Estadual:') !!}
                                    {!! Form::number('inscricao_estadual', null, ['class' => 'form-control', 'id' => 'inscricao_estadual']) !!}
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('email', 'Email:') !!}
                                    {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                                </div>
                                <div class="col-md-2">
                                    {!! Form::label('telefone', 'Telefone:') !!}
                                    {!! Form::text('telefone', null, ['class' => 'form-control', 'id' => 'telefone']) !!}
                                </div>
                                <div class="col-md-3">
                                    {!! Form::label('telefone_secundario', 'Celular:') !!}
                                    {!! Form::text('telefone_secundario', null, ['class' => 'form-control', 'id' => 'celular']) !!}
                                </div>
                           </div>
                    </fieldset>
                   <br />
                   <fieldset>
                        <legend>Logo</legend>
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <input type="file" name="file" id="file" class="inputfile" accept="image/*"/>
                                    <label for="file"> <i class="fa fa-cloud-upload" aria-hidden="true"></i> Selecione Imagem</label>
                                    @if($image != null )
                                        <div class="img-preview">
                                            <img src="{{ asset('storage/imagem/clinica/'.$image) }}" />
                                        </div>
                                    @endif
                                </center>
                            </div>
                        </div>
                    </fieldset>
                   <br />            
                    <fieldset>
                        <legend>Endereço</legend>
                        <br />
                        <div class="row">
                            <div class="col-md-9">
                                {!! Form::label('logradouro', 'Logradouro:') !!}
                                {!! Form::text('logradouro', null, ['class' => 'form-control', 'id' => 'logradouro']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::label('cep', 'CEP:') !!}
                                {!! Form::text('cep', null, ['class' => 'form-control', 'id' => 'cep']) !!}
                            </div>
                            <div class="col-md-1">
                                {!! Form::label('numero', 'Nº:') !!}
                                {!! Form::text('numero', null, ['class' => 'form-control', 'id' => 'numero']) !!}
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::label('bairro', 'Bairro:') !!}
                                {!! Form::text('bairro', null, ['class' => 'form-control', 'id' => 'bairro']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('cidade', 'Cidade:') !!}
                                {!! Form::text('cidade', null, ['class' => 'form-control', 'id' => 'cidade']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::label('complemento', 'Complemento:') !!}
                                {!! Form::text('complemento', null, ['class' => 'form-control', 'id' => 'complemento']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::label('uf_id', 'UF:') !!}
                                {!! Form::select('uf_id', $estados, null, ['class' => 'form-control', 'id' => 'uf']) !!}
                            </div>
                        </div>
                        <br/>
                    </fieldset>
                </div>
                <div class="box-footer">
                    <button type="button" class="btn btn-default btn-flat" id="fechar-clinica"> <i class="fa fa-ban" aria-hidden="true"></i></button>
                    <button type="submit" class="btn btn-success btn-flat pull-right" id="salvar-clinica"> <i class="fa fa-check" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    @includeIf('layouts.partials.modal', ['idModal' => 'modal-form-clinica', 'idContent' => 'content-modal-clinica'])
@stop

@section('script-footer')
    <script src=" {{ asset('js/pages/clinica.js') }}"></script>
@stop