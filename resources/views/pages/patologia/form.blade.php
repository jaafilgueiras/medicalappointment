@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center"> <i class="fa fa-heartbeat" aria-hidden="true"></i> {{ (isset($patologia)) ? 'Editar' : 'Adicionar' }} Patologia</h3>
                </div>
            </div>
        </div>
    </div>
    @if(isset($patologia))
        {!! Form::model($patologia, ['action' => ('PatologiaController@store'), 'id' => 'form-patologia']) !!}
    @else
        {!! Form::open(['action' => ('PatologiaController@store'), 'id' => 'form-patologia']) !!}
    @endif
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                             <div id="notify"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            {!! Form::hidden('id', null, ['id' => 'id']) !!}
                            {!! Form::hidden('patologias', null, ['id' => 'patologias-values']) !!}
                            <div class="col-md-4">
                                {!! Form::label('nome', 'Nome:') !!}
                                {!! Form::text('nome', null, ['class' => 'form-control', 'id' => 'nome']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('nome_cientifico', 'Nome Científico:') !!}
                                {!! Form::text('nome_cientifico', null, ['class' => 'form-control', 'id' => 'nome_cientifico']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('origem', 'Origem:') !!}
                                {!! Form::text('origem', null, ['class' => 'form-control', 'id' => 'origem']) !!}
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('descricao', 'Descrição:') !!}
                                {!! Form::textArea('descricao', null, ['class' => 'form-control', 'id' => 'descricao', 'rows' => 4]) !!}
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-5">
                                {!! Form::label('', 'Sintoma:') !!}
                                <div class="input-group">
                                    {!! Form::select('sintoma_id', $sintomas , null, ['class' => 'form-control', 'id' => 'select_sintomas']) !!}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat" id="add-sintoma"><i class="fa fa-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                {!! Form::label('', 'Consequência:') !!}
                                <div class="input-group">
                                    {!! Form::select('consequencia_id', $consequencias , null, ['class' => 'form-control', 'id' => 'select_consequencias']) !!}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat" id="add-consequencia"><i class="fa fa-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-info btn-flat btn-resposive-input-group" id="add-patologias"><i class="fa fa-plus"></i> Adicionar</button>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover table-max-height">
                                    <thead>
                                        <tr>
                                            <th>Sintomas</th>
                                            <th>Consequências</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-body-patologias"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default btn-flat" id="fechar-patologia"> <i class="fa fa-ban" aria-hidden="true"></i></button>
                        <button type="submit" class="btn btn-success btn-flat pull-right" id="salvar-patologia"> <i class="fa fa-check" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@stop

@section('script-footer')
    <script src=" {{ asset('js/pages/patologia.js') }}"></script>
    <script type="text/javascript">
        loadPatologias();
    </script>
@stop