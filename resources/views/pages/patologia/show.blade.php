@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
            	<div class="box-header with-border">
		        	<h3 class="text-center"> <i class="fa fa-heartbeat" aria-hidden="true"></i> Patologia {{ $patologia->nome}}</h3>
            	</div>
            	<div class="box-body">
            		<br />
            		<div class="row">
		                <div class="col-md-4" >
		                    <p><strong>Nome:</strong> {{ $patologia->nome }}</p>
		                </div>
		                <div class="col-md-4">
		                    <p><strong>Nome Cientifico :</strong> {{ $patologia->nome_cientifico }}</p>
		                </div>
		                <div class="col-md-4">
		                    <p><strong>Origem :</strong> {{ $patologia->origem }}</p>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <p><strong>Descrição:</strong> {{ $patologia->descricao }}</p>
		                </div>
		            </div>
		            <br />
            		<div class="row">
            			<div class="col-md-12">
            				<div class="box box-success box-solid">
					            <div class="box-header with-border">
					              <h3 class="box-title"><i class="fa fa-thermometer-half"></i> Sintomas</h3>

					              <div class="box-tools pull-right">
					                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					                </button>
					              </div>
					            </div>
					            <div class="box-body">
					            	<div class="row">
					            		<div class="col-md-5">
						            		<p><b>Nome</b></p>	
						            	</div>
						            	<div class="col-md-7">
						            		<p><b>Descrição</b></p>	
						            	</div>
					            	</div>
					            	<div class="row">
					            		@foreach($sintomas as $row)
						            		<div class="col-md-5">
								            	<p>{{ $row->nome}}</p>
								            </div>
						            		<div class="col-md-7">
						            			<p>{{ $row->nome}}</p>
						            		</div>
						                @endforeach	
					            	</div>
					            </div>
					        </div>
            			</div>
            		</div>
            		<div class="row">
            			<div class="col-md-12">
            				<div class="box box-success box-solid">
					            <div class="box-header with-border">
					              <h3 class="box-title"> <i class="fa fa-medkit" aria-hidden="true"></i> Consequências</h3>

					              <div class="box-tools pull-right">
					                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					                </button>
					              </div>
					            </div>
					            <div class="box-body">
					            	<div class="row">
					            		<div class="col-md-5">
						            		<p><b>Nome</b></p>	
						            	</div>
						            	<div class="col-md-7">
						            		<p><b>Descrição</b></p>	
						            	</div>
					            	</div>
					            	<div class="row">
					            		@foreach($consequencias as $row)
						            		<div class="col-md-5">
								            	<p>{{ $row->nome}}</p>
								            </div>
						            		<div class="col-md-7">
						            			<p>{{ $row->nome}}</p>
						            		</div>
						                @endforeach	
					            	</div>
					            </div>
					        </div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
@stop

@section('script-footer')
    <script src=" {{ asset('js/pages/patologia.js') }}"></script>
@stop