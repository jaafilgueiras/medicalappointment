@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center"> <i class="fa fa-wheelchair" aria-hidden="true"></i> Paciente </h3>
                </div>
            </div>
        </div>
    </div>

    @if(isset($paciente))
        {!! Form::model($paciente, ['action' => ('PacienteController@store'), 'method' => 'post', 'id' => 'form-paciente', 'files' => true]) !!}
    @else
        {!! Form::open(['action' => ('PacienteController@store'), 'method' => 'post' , 'id' => 'form-paciente', 'files' => true]) !!}
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notify"></div>
                        </div>
                    </div>
                    <fieldset>
                        <legend>Dados Pessoais</legend>
                        <br />
                        <div class="row">
                            {!! Form::hidden('id', null, ['id' => 'id']) !!}
                            <div class="col-md-6">
                                {!! Form::label('nome', 'Nome:') !!}
                                {!! Form::text('nome', null, ['class' => 'form-control', 'id' => 'nome']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::label('data_nascimento', 'Data Nascimento:') !!}
                                {!! Form::text('data_nascimento', null, ['class' => 'form-control', 'id' => 'data-nascimento']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::label('cpf', 'CPF:') !!}
                                {!! Form::text('cpf', null, ['class' => 'form-control', 'id' => 'cpf']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::label('telefone', 'Telefone:') !!}
                                {!! Form::text('telefone', null, ['class' => 'form-control', 'id' => 'telefone']) !!}
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('nome_acompanhante', 'Nome Acompanhante:') !!}
                                {!! Form::text('nome_acompanhante', null, ['class' => 'form-control', 'id' => 'nome-acompanhante']) !!}
                            </div>
                        </div>
                        <br/>
                    </fieldset>
                    <br/>
                    <fieldset>
                        <legend>Endereço</legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="notify"></div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            {!! Form::hidden('id', null, ['id' => 'id']) !!}
                            <div class="col-md-3">
                                {!! Form::label('cep', 'CEP:') !!}
                                {!! Form::text('cep', null, ['class' => 'form-control', 'id' => 'cep']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::label('logradouro', 'Logradouro:') !!}
                                {!! Form::text('logradouro', null, ['class' => 'form-control', 'id' => 'logradouro']) !!}
                            </div>
                            <div class="col-md-1">
                                {!! Form::label('numero', 'Nº') !!}
                                {!! Form::text('numero', null, ['class' => 'form-control', 'id' => 'numero']) !!}
                            </div>

                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-3">
                                {!! Form::label('complemento', 'Complemento:') !!}
                                {!! Form::text('complemento', null, ['class' => 'form-control', 'id' => 'complemento']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('cidade', 'Cidade:') !!}
                                {!! Form::text('cidade', null, ['class' => 'form-control', 'id' => 'cidade']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('bairro', 'Bairro:') !!}
                                {!! Form::text('bairro', null, ['class' => 'form-control', 'id' => 'bairro']) !!}
                            </div>
                            <div class="col-md-1">
                                {!! Form::label('uf_id', 'UF:') !!}
                                {!! Form::select('uf_id', $estados, null, ['class' => 'form-control', 'id' => 'uf']) !!}
                            </div>
                        </div>
                        <br/>
                    </fieldset>
                </div>
                <div class="box-footer">
                    <button type="button" class="btn btn-default btn-flat" id="fechar-paciente"> <i class="fa fa-ban" aria-hidden="true"></i></button>
                    <button type="submit" class="btn btn-success btn-flat pull-right" id="salvar-paciente"> <i class="fa fa-check" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    {{--@includeIf('layouts.partials.modal', ['idModal' => 'modal-form-clinica', 'idContent' => 'content-modal-clinica'])--}}
@stop

@section('script-footer')
    <script src=" {{ asset('js/pages/paciente.js') }}"></script>
@stop