<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('App\Models\Role');
    }
 
    public function hasPermission(Permission $permission)
    {
        return $this->hasAnyRoles($permission->roles);
    }
 
    public function hasAnyRoles($roles)
    {
        if( is_array($roles) || is_object($roles) )
        {
            foreach ($roles as  $role) 
            {
                return $this->hasAnyRoles($role);
            }
        }
         
        return $this->roles->contains('name', $roles);
    }

    public function rules(){
        return $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }

    public $mensages = [
        'name.required' => 'O Nome é obrigatório.',
        'name.max' => 'O tamanho máximo do nome é de 255 caracteres',
        'email.required' => 'O E-mail é obrigatório.',
        'email.max' => 'O tamanho máximo do e-mail é de 255 caracteres',
        'email.email' => 'E-mail em um formato inválido',
        'email.unique' => 'E-mail já cadastrado para outro Usuário.'
    ];
}
