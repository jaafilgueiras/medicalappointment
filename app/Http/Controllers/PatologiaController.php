<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patologia;
use App\Models\Sintoma;
use App\Models\Consequencia;

class PatologiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Patologia::orderBy('id')->paginate(10);

        return view('pages.patologia.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sintomas       = toSelect(Sintoma::all()->toArray(), 'id', 'nome');
        $consequencias  = toSelect(Consequencia::all()->toArray(), 'id', 'nome');

        return view('pages.patologia.form', compact('sintomas', 'consequencias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id         = $request->input('id');
        $patologias = $request->input('patologias');

        $patologia  = Patologia::find($id);

        if(!$patologia)
        {
            $patologia = new Patologia();
        }

        $patologia->fill($request->all());

        $validate = validator($request->all(), $patologia->rules(), $patologia->mensages);
            
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        if(count($patologias) == null || count($patologias) < 1)
        {
            return response()->json(['success' => false, 'msg' => 'Nenhuma sintoma nem consequência foi adicionado a lista.']);
        }

        $save = $patologia->save();

        if($save) 
            {

                if(isset($patologias))
                {
                    $patologias = json_decode($patologias);

                    \DB::table('sintoma_consequencia_sintoma')->where('patologia_id', $patologia->id)
                    ->delete();

                    foreach($patologias as $patologias)
                    {
                        \DB::table('sintoma_consequencia_sintoma')
                            ->insert(['patologia_id' => $patologia->id, 'consequencia_id' => $patologias->consequencia_id, 'sintoma_id' => $patologias->sintoma_id]);
                    }
                }
                return response()->json(['success' => true, 'msg' => 'Patologia salvo com sucesso!']);
            }else
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao salvar Patologia!']);
            }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Patologia $patologia)
    {
        $sintomas       = toSelect(Sintoma::all()->toArray(), 'id', 'nome');
        $consequencias  = toSelect(Consequencia::all()->toArray(), 'id', 'nome');

        return view('pages.patologia.form', compact('sintomas', 'consequencias', 'patologia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patologia      = Patologia::where('id', $id)->first();
        
        $sintomas       = \DB::table('sintoma_consequencia_sintoma')
                             ->join('sintoma', 'sintoma.id', '=', 'sintoma_consequencia_sintoma.sintoma_id')
                             ->where('sintoma_consequencia_sintoma.patologia_id', $id)
                             ->get();

        $consequencias  = \DB::table('sintoma_consequencia_sintoma')
                             ->join('consequencia', 'consequencia.id', '=', 'sintoma_consequencia_sintoma.consequencia_id')
                             ->where('sintoma_consequencia_sintoma.patologia_id', $id)
                             ->get();
        return view('pages.patologia.show', compact('sintomas', 'consequencias', 'patologia'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       try
        {
            $id = $request->input('id');

            \DB::table('sintoma_consequencia_sintoma')->where('patologia_id', $id)->delete();
            
            $delete = Patologia::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Patologia excluída com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir Patologia.']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Patologia! '.$e]);
        }
    }

    public function getPatologias($idPatologia){
        
        $result = \DB::table('sintoma_consequencia_sintoma')
                    ->join('patologia', 'patologia.id', '=', 'sintoma_consequencia_sintoma.patologia_id')
                    ->join('sintoma', 'sintoma.id', '=', 'sintoma_consequencia_sintoma.sintoma_id')
                    ->join('consequencia', 'consequencia.id', '=', 'sintoma_consequencia_sintoma.consequencia_id')
                    ->select('sintoma.id as sintoma_id', 'sintoma.nome as name_sintoma', 'consequencia.id as consequencia_id', 'consequencia.nome as name_consequencia')
                    ->where('sintoma_consequencia_sintoma.patologia_id', $idPatologia)
                    ->get();
                    return response()->json($result);

        return $result;
    }
}
