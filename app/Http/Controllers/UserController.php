<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $result = User::orderBy('id')->paginate(10);

        return view('pages.usuario.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $funcoes = toSelect(Role::all()->toArray(), 'id', 'name');

        return view('pages.usuario.form', compact('funcoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id              = $request->input('id');
        $password        = trim($request->input('password'));
        $repeat_password = $request->input('repeat_password');

        $usuario = User::where('id', $id)->first();

        if (!$usuario){
            $usuario = new User();
        }

        $usuario->fill($request->all());

        if(empty($id)){

            if (empty($password)) {

                return response()->json(['success' => false, 'msg'=> 'A senha é Obrigatória.']);

            } elseif ($password === $repeat_password) {

                $usuario->password = bcrypt($password);

            } else {
                return response()->json(['success' => false, 'msg'=> 'A confirmação da senha não corresponde.']);
            }

        } elseif($password != null) {

            if ($password === $repeat_password) {

                $usuario->password = bcrypt($password);

            } else {
                return response()->json(['success' => false, 'msg'=> 'A confirmação da senha não corresponde.']);
            }

        }

        $validate = validator($request->all(), $usuario->rules(), $usuario->mensages);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayToValidator($validate->errors())]);
        }

        $save = $usuario->save();

        if($save) {

            $funcoes = $request->input('funcoes');

            if(isset($funcoes))
            {
                $funcoes = json_decode($funcoes);

                \DB::table('role_user')->where('user_id', $id)
                ->delete();

                foreach($funcoes as $funcao)
                {
                    \DB::table('role_user')->insert(['role_id' => $funcao->id, 'user_id' => $usuario->id]);
                }
            }
            return response()->json(['success' => true, 'msg' => 'Função salvo com sucesso!']);

        return response()->json(['success' => true, 'msg' => 'Usuário salvo com sucesso.']);
        }else{
            return response()->json(['success' => false, 'msg' => 'Erro ao salvar Usuário.']);
        }


    }

    public function edit(User $usuario)
    {
        $funcoes = toSelect(Role::all()->toArray(), 'id', 'name');

        return view('pages.usuario.form', compact('usuario','funcoes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
     
     try
        {
            $id = $request->input('id');

            \DB::table('role_user')->where('user_id', $id)->delete();
            
            $delete = User::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Usuário excluído com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir Usuário.']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Usuário! '.$e]);
        }

    }

    public function getFuncoes($idFuncao)
    {
        $result = \DB::table('role_user')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->select('role_user.id', 'role_user.role_id', 'roles.name')
                    ->where('role_user.user_id', $idFuncao)
                    ->get();

        return $result;
    }
}
