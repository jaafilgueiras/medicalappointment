<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estados;
use App\Models\Clinica;
use Storage;

class ClinicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clinica   = Clinica::first();
        $estados    = toSelect(Estados::all()->toArray(), 'id', 'uf');

        $folder    =  public_path().'/storage/imagem/clinica';

        if(!\File::exists($folder))
        {
            Storage::makeDirectory('public/imagem/clinica');
        }

        $file      = \File::allFiles(public_path().'/storage/imagem/clinica/');
        $image     = $file == null ? $file : $file[0]->getFilename();

        return view('pages.clinica.index', compact('clinica','estados','image'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        $cnpj = $request->input('cnpj');
        $cep = $request->input('cep');
        $telefone = $request->input('telefone');
        $celular = $request->input('telefone_secundario');

        $clinica = Clinica::find($id);

        if(!$clinica){
            $clinica = new Clinica();
        }

        $clinica->fill($request->all());

        $clinica->cnpj = str_replace(array('.', '/', '-', '_'), '', $cnpj);
        $clinica->cep = str_replace(array('-', '_'), '', $cep);
        $clinica->telefone = str_replace(array('(',')', ' ', '-', '_'), '', $telefone);
        $clinica->telefone_secundario = str_replace(array('(',')', ' ', '-', '_'), '', $celular);

        $validate = validator($request->all(), $clinica->rules(), $clinica->msgRules);
            
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        if(strlen($clinica->cnpj) != 14){
            return response()->json(['success' => false, 'msg' => 'CNPJ inválido.']);
        }

        if(strlen($clinica->cep) != 8){
            return response()->json(['success' => false, 'msg' => 'Cep inválido.']);
        }

        if(strlen($clinica->telefone) != 10){
            return response()->json(['success' => false, 'msg' => 'Telefone inválido.']);
        }

        if(strlen($clinica->telefone_secundario) < 10 || strlen($clinica->telefone_secundario) > 11){
            return response()->json(['success' => false, 'msg' => 'Celular inválido.']);
        }

        $save = $clinica->save();

        if($save)
        {
            if($request->hasFile('file')){
                $file      = \File::allFiles(public_path().'/storage/imagem/clinica/');

                if($file != null){
                    $fileExist = $file[0]->getFilename();
                    \File::delete(public_path().'/storage/imagem/clinica/'.$fileExist);
                }

                $file       = $request->file('file');
                $folder     =  public_path().'/storage/imagem/clinica';
                $extensions = ["jpeg", "jpg", "png"];
                
                if(!\File::exists($folder))
                {
                    Storage::makeDirectory('public/imagem/clinica');
                }

                if(in_array($file->getClientOriginalExtension(), $extensions)){
                    if($file->getSize() < 69468){
                        $newName = '1.'.$file->getClientOriginalExtension();
                        if(!$file->move($folder, $newName)){
                            return response()->json(['success' => false, 'msg' => 'Erro ao carregar a imagem']);
                        }
                    }else{
                        return response()->json(['success' => false, 'msg' => 'Tamanho da imagem é maior que 67KB.']);
                    }
                }else{
                    return response()->json(['success' => false, 'msg' => 'Erro! Extensões permitidas: jpg, jpeg e png.']);
                }


            }
            return response()->json(['success' => true, 'msg' => 'Clinica salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar clinica!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
