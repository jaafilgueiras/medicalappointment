<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;

class FuncaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Role::orderBy('id')->paginate(10);
        return view('pages.funcao.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = toSelect(Permission::all()->toArray(), 'id', 'name');

        return view('pages.funcao.form', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
        
            $id = $request->input('id');
            
            $role = Role::find($id);
            
            if(!$role)
            {
                $role = new Role();
            }

            $role->fill($request->all());

            $role->name = strtoupper($request->name);
            
            $validate = validator($request->all(), $role->rules(), $role->mensages);
            
            if($validate->fails())
            {
                return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
            }

            $permissoes = $request->input('permissoes');

            if(count($permissoes) == null || count($permissoes) < 1)
            {
                return response()->json(['success' => false, 'msg' => 'Nenhuma permissão foi adicionado a lista.']);
            }


            $save = $role->save();
            
            if($save) 
            {

                if(isset($permissoes))
                {
                    $permissoes = json_decode($permissoes);

                    \DB::table('permission_role')->where('role_id', $id)
                    ->delete();

                    foreach($permissoes as $permission)
                    {
                        \DB::table('permission_role')->insert(['permission_id' => $permission->id, 'role_id' => $role->id]);
                    }
                }
                return response()->json(['success' => true, 'msg' => 'Função salvo com sucesso!']);
            }else
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao salvar Função!']);
            }
        }
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao salvar Função! '.$e]);
        }
    }

    public function edit( Role $funcao )
    {
        $permissions = toSelect(Permission::all()->toArray(), 'id', 'name');
        
        return view('pages.funcao.form', compact('funcao', 'permissions'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');

            \DB::table('permission_role')->where('role_id', $id)->delete();
            
            $delete = Role::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Função excluída com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir Função.']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Função! '.$e]);
        }
    }

    public function getPermissions($idRole){
        
        $result = \DB::table('permission_role')
                        ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                        ->select('permission_role.id', 'permission_role.permission_id', 'permissions.description')
                        ->where('permission_role.role_id', $idRole)
                        ->get();

        return $result;
    }
}
