<?php

namespace App\Http\Controllers;

use App\Models\TipoConsulta;
use Illuminate\Http\Request;

class TipoConsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = TipoConsulta::orderBy('id')->paginate(10);
        
        return view('pages.tipo-consulta.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.tipo-consulta.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        
        $tipoConsulta = TipoConsulta::find($id);
        
        if(!$tipoConsulta)
        {
            $tipoConsulta = new TipoConsulta();
        }

        $tipoConsulta->fill($request->all());
        
        $validate = validator($request->all(), $tipoConsulta->rules(), $tipoConsulta->mensages);
        
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $tipoConsulta->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Tipo de Consulta salvo com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Tipo de Consulta!']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoConsulta $tipoConsulta)
    {
        return view('pages.tipo-consulta.form', compact('tipoConsulta'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');
            
            $delete = TipoConsulta::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Tipo de Consulta excluído com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possivel excluir o Tipo de Consulta!']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Tipo de Consulta! '.$e]);
        }
    }
}
