<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Consequencia;

class ConsequenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter  = $request->input('filtro');

        $result = Consequencia::orderBy('id');

        if ($filter){
            $result = $result->where('descricao','ilike', '%'.$filter.'%')
                             ->orWhere('nome','ilike', '%'.$filter.'%');
        }

        $result = $result->paginate('10');
        
        return view('pages.consequencia.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.consequencia.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        
        $consequencia = Consequencia::find($id);
        
        if(!$consequencia)
        {
            $consequencia = new Consequencia();
        }

        $consequencia->fill($request->all());
        
        $validate = validator($request->all(), $consequencia->rules(), $consequencia->mensages);
        
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $consequencia->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Consequencia salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Consequencia!']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Consequencia $consequencia)
    {
        return view('pages.consequencia.form', compact('consequencia'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');
            
            $delete = Consequencia::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Consequencia excluída com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possivel excluir a Consequencia!']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Consequencia! '.$e]);
        }
    }
}
