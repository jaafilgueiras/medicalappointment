<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sintoma;

class SintomaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Sintoma::orderBy('id')->paginate(10);
        
        return view('pages.sintoma.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.sintoma.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        
        $sintoma = Sintoma::find($id);
        
        if(!$sintoma)
        {
            $sintoma = new Sintoma();
        }

        $sintoma->fill($request->all());
        
        $validate = validator($request->all(), $sintoma->rules(), $sintoma->mensages);
        
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $sintoma->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Sintoma salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Sintoma!']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sintoma $sintoma)
    {
        return view('pages.sintoma.form', compact('sintoma'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');
            
            $delete = Sintoma::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Sintoma excluída com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possivel excluir a Sintoma!']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Sintoma! '.$e]);
        }
    }
}
