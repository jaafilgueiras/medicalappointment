<?php

namespace App\Http\Controllers;

use App\Models\Especialidade;
use Illuminate\Http\Request;

class EspecialidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter  = $request->input('filtro');

        $result = Especialidade::orderBy('id');

        if ($filter){
            $result = $result->where('descricao','ilike', '%'.$filter.'%')
                             ->orWhere('nome','ilike', '%'.$filter.'%');
        }

        $result = $result->paginate('10');
        
        return view('pages.especialidade.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.especialidade.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        
        $especialidade = Especialidade::find($id);
        
        if(!$especialidade)
        {
            $especialidade = new Especialidade();
        }

        $especialidade->fill($request->all());
        
        $validate = validator($request->all(), $especialidade->rules(), $especialidade->mensages);
        
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $especialidade->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Especialidade salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Especialidade!']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Especialidade $especialidade)
    {
        return view('pages.especialidade.form', compact('especialidade'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');
            
            $delete = Especialidade::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Especialidade excluída com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possivel excluir a Especialidade!']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Especialidade! '.$e]);
        }
    }
}
