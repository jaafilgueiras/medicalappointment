<?php

namespace App\Http\Controllers;

use App\Models\Especialidade;
use App\Models\Paciente;
use App\Models\TipoConsulta;
use Illuminate\Http\Request;
use App\Models\Clinica;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Consequencia;
use App\Models\Sintoma;
use App\User;

class PanelController extends Controller
{
    public function index(){
        return view('pages.index');
    }

    public function sistema(){

        $clinica        = Clinica::count();
        $funcoes        = Role::count();
        $permissoes     = Permission::count();
        $usuarios       = User::count();
        $consequencia   = Consequencia::count();
        $sintoma        = Sintoma::count();
        $especialidade  = Especialidade::count();
        $tipoConsulta   = TipoConsulta::count();
        $paciente       = Paciente::count();

        return view('pages.sistema.index', compact('clinica', 'permissoes', 'funcoes', 'usuarios', 'consequencia', 'sintoma',
                                                   'especialidade', 'tipoConsulta', 'paciente'));
    }
}
