<?php

namespace App\Http\Controllers;

use App\Models\Estados;
use App\Models\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Paciente::orderBy('id')->paginate(10);
        
        return view('pages.paciente.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados    = toSelect(Estados::all()->toArray(), 'id', 'uf');

        return view('pages.paciente.form', compact('estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        $data_nascimento = $request->input('data_nascimento');
        $cpf = $request->input('cpf');
        $cep = $request->input('cep');
        $telefone = $request->input('telefone');

        
        $paciente = Paciente::find($id);
        
        if(!$paciente)
        {
            $paciente = new Paciente();
        }

        $paciente->fill($request->all());

        $paciente->cpf = str_replace(array('.', '-', '_'), '', $cpf);
        $paciente->cep = str_replace(array('-', '_'), '', $cep);
        $paciente->telefone = str_replace(array('(',')', ' ', '-', '_'), '', $telefone);
        $paciente->data_nascimento = dateToSave($data_nascimento);
        
        $validate = validator($request->all(), $paciente->rules(), $paciente->mensages);
        
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

//        INICIANDO A VALIDAÇÃO DOS CAMPOS COM MÁSCARA

        if(strlen($paciente->cpf) != 11){
            return response()->json(['success' => false, 'msg' => 'CPF inválido!']);
        }

        if(strlen($paciente->cep) != 8){
            return response()->json(['success' => false, 'msg' => 'CEP inválido!']);
        }

        if(strlen($paciente->telefone) < 10 || strlen($paciente->telefone) > 11){
            return response()->json(['success' => false, 'msg' => 'Telefone inválido!']);
        }

        $save = $paciente->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Paciente salvo com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Paciente!']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        $estados    = toSelect(Estados::all()->toArray(), 'id', 'uf');

        return view('pages.paciente.form', compact('paciente', 'estados'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');
            
            $delete = Paciente::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Paciente excluído com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possivel excluir o Paciente!']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Paciente! '.$e]);
        }
    }
}
