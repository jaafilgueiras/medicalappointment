<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consequencia extends Model
{
    protected $table = 'consequencia';
    protected $fillable = ['nome', 'descricao'];

    public function rules() { 
        return [ 
            'nome' => 'required|unique:consequencia,nome'. (($this->id) ? ', ' . $this->id : '')
        ];
    }
    
    public $mensages = [
        'nome.required' => 'Nome é obrigatório!',
        'nome.unique'   => 'Nome já cadastrado!'
    ];
}
