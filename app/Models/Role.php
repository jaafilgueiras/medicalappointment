<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    Protected $table = 'roles';
    protected $fillable = ['name', 'description'];
    
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission');
    }

    public function rules() { 
        return [ 
            'name' => 'required|unique:roles,name'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }
    
    public $mensages = [
        'name.required' => 'O nome é obrigatório!',
        'name.unique' => 'Esse nome já está cadastrado!'
    ];
}
