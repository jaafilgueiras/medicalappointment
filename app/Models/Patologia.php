<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patologia extends Model
{
    protected $table = 'patologia';
    protected $fillable = [
        'nome',
        'nome_cientifico',
        'origem',
        'descricao'
    ];

    public function rules() { 
        return [ 
            'nome'              => 'required',
            'nome_cientifico'   => 'required',
            'origem'            => 'required'
        ];
    }
    
    public $mensages = [
        'nome.required'             => 'Nome é obrigatório!',
        'nome_cientifico.required'  => 'Nome Científico é obrigatório!',
        'origem.required'           => 'Origem é obrigatório!'
    ];
}
