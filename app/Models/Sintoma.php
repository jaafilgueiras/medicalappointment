<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sintoma extends Model
{
    protected $table = 'sintoma';
    protected $fillable = ['nome', 'descricao'];

    public function rules() { 
        return [ 
            'nome' => 'required|unique:sintoma,nome'. (($this->id) ? ', ' . $this->id : '')
        ];
    }
    
    public $mensages = [
        'nome.required' => 'Nome é obrigatório!',
        'nome.required' => 'Nome já cadastrado!'
    ];
}
