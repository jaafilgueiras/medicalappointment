<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Especialidade extends Model
{
    protected $table = 'especialidade';
    protected $fillable = ['nome', 'descricao'];

    public function rules() { 
        return [ 
            'nome' => 'required|unique:especialidade,nome'. (($this->id) ? ', ' . $this->id : '')
        ];
    }
    
    public $mensages = [
        'nome.required' => 'Nome é obrigatório!',
        'nome.unique'   => 'Nome já cadastrado!'
    ];
}
