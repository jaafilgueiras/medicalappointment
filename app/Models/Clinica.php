<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    protected $table = 'clinica';
    protected $fillable = [
        'cnpj',
        'nome_fantasia',
        'razao_social',
        'inscricao_estadual',
        'email',
        'numero',
        'telefone', 
        'logradouro',
        'cep', 
        'uf_id',
        'cidade',
        'bairro',
        'complemento',
        'telefone_secundario'
    ];

    public function uf()
    {
      return $this->belongsTo('App\Models\Estados', 'uf_id', 'id');
    }

    public function rules() { 
        
        return [ 
            'cnpj'              => 'required|unique:clinica,cnpj'. (($this->id) ? ', ' . $this->id : ''),
            'nome_fantasia'     => 'required',
            'razao_social'      => 'required',
            'email'             => 'required',
            'numero'            => 'required',
            'telefone'          => 'required',
            'logradouro'        => 'required',
            'cep'               => 'required',
            'uf_id'             => 'required|not_in:0',
            'cidade'            => 'required',
            'bairro'            => 'required',
            'complemento'       => 'required',
                    
        ];
    }
            
    public $msgRules = [
        'cnpj.required'              => 'CNPJ é obrigatório!',
        'cnpj.unique'                => 'CNPJ já está cadastrado!',
        'nome_fantasia.required'     => 'Nome Fantasia é obrigatório!',
        'razao_social.required'      => 'Razão Social é obrigatório!',
        'email.required'             => 'Email é obrigatório!',
        'numero.required'            => 'Numero é obrigatório!',
        'telefone.required'          => 'Telefone é obrigatório!',
        'logradouro.required'        => 'Logradouro é obrigatório!',
        'cep.required'               => 'Cep é obrigatório!',
        'uf_id.required'             => 'UF é obrigatório!',
        'uf_id.not_in'               => 'UF não selecionado!',
        'cidade.required'            => 'Cidade é obrigatório!',
        'bairro.required'            => 'Bairro é obrigatório!',
        'complemento.required'       => 'Complemento é obrigatório!',
    ];

}
