<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'paciente';
    protected $fillable = ['nome',
                           'data_nascimento',
                           'nome_acompanhante',
                           'numero',
                           'telefone',
                           'logradouro',
                           'cep',
                           'uf_id',
                           'cidade',
                           'bairro',
                           'complemento'];

    public function rules() { 
        return [ 
            'nome' => 'required|unique:paciente,nome'. (($this->id) ? ', ' . $this->id : ''),
            'data_nascimento' => 'required',
            'nome_acompanhante' => 'required',
            'numero' => 'required',
            'telefone' => 'required',
            'logradouro' => 'required',
            'cep' => 'required',
            'uf_id' => 'required|not_in:0',
            'cidade' => 'required',
            'bairro' => 'required'
        ];
    }
    
    public $mensages = [
        'nome.required'            => 'Nome é obrigatório!',
        'nome.unique'              => 'Nome já cadastrado!',
        'data_nascimento.required' => 'Data Nascimento é obrigatória!',
        'numero.required'          => 'Número é obrigatório!',
        'telefone.required'        => 'Telefone é obrigatório!',
        'logradouro.required'      => 'Logradouro é obrigatório!',
        'cep.required'             => 'CEP é obrigatório!',
        'uf_id.required'           => 'UF é obrigatória!',
        'uf_id.not_in'             => 'UF não selecionado!',
        'cidade.required'          => 'Cidade é obrigatória!',
        'bairro.required'          => 'Bairro é obrigatório!'
    ];
}
