<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoConsulta extends Model
{
    protected $table = 'tipo_consulta';
    protected $fillable = ['descricao'];

    public function rules() { 
        return [ 
            'descricao' => 'required|unique:tipo_consulta,descricao'. (($this->id) ? ', ' . $this->id : '')
        ];
    }
    
    public $mensages = [
        'descricao.required' => 'Descrição é obrigatória!',
        'descricao.unique'   => 'Descrição já cadastrada!'
    ];
}
