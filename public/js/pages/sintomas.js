//functions
function createSintomas(){
    $.get(urlBase + "panel/sintoma/create", function(response){
        $("#content-modal-sintoma").html(response);
        $('#modal-form-sintoma').modal('show');        
    });
}

function editSintomas(id){
    $.get(urlBase + "panel/sintoma/edit/" + id, function(response){
        $("#content-modal-sintoma").html(response);
        $('#modal-form-sintoma').modal('show');
    });
}

function destroySintomas(id, name){
    $.confirm({
        title: 'Excluir Sintoma!',
        content: 'Deseja realmente excluir a Sintoma <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/sintoma/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){
    $("#content-modal-sintoma").on("click", "#fechar-sintoma", function() {
        $('#modal-form-sintoma').modal('toggle');
    });

    $('#content-modal-sintoma').on('submit', '#form-sintoma', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-sintoma").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/sintoma/store",
            data: $("#form-sintoma").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){ 
                        $('#modal-form-sintoma').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-sintoma").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});
