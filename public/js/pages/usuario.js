function createUsuario(){
    $.get(urlBase + "panel/usuario/create", function(response){
        $("#content-modal-usuario").html(response);
        $('#modal-form-usuario').modal('show');        
    });
}

function editUsuario(id){
    $.get(urlBase + "panel/usuario/edit/" + id, function(response){
        $("#content-modal-usuario").html(response);
        $('#modal-form-usuario').modal('show');
    });
}

//carregar as permissões
function loadFuncoes()
{
    var id = $("#id").val();

    if(id > 0 || id != '')
    {
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});

        $.ajax({
            type: "GET",
            url: urlBase + "panel/usuario/get-funcoes/" + id,
            data: {},
            success: function(data){
                if(data)
                {
                    console.log(data);
                    data.forEach(function(item, index) {
                        addFuncao(getFuncao(), item.role_id,  item.name, false);
                    });
                }
                
                waitingDialog.hide();
            }, error: function(){
                notifyAlert('danger', 'Erro ao carregar Funções.');
                waitingDialog.hide();
            }
        });
    }
}

//set valor na table as permissões
function setFuncao(funcao)
{
    $("#funcoes-values").val(JSON.stringify(funcao));
}
//adicionar permissão
function addFuncao(array, idFuncao, nameFuncao, deletar)
{
    array.push({ id: idFuncao, name: nameFuncao , delete : deletar});
    setFuncao(array);
    $("#table-body-funcao").append('<tr><td>'+ nameFuncao +'</td><td style="width: 5%;"><center><button type="button" class="btn btn-danger btn-flat btn-xs" onclick="removeFuncao(this, '+ idFuncao +');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></center></td></tr>');
}
//remover permissão
function removeFuncao(element, id)
{
    var funcoes = getFuncao();

    funcoes.forEach(function(item, index, object) {
        if(item.id == id)
        {
            item.delete = true;
            object.splice(index, 1);
            
            $(element).closest("tr").remove();
        }
    });
    console.log(funcoes);
    setFuncao(funcoes);
}

function getFuncao()
{
    var funcoes = $("#funcoes-values").val();
    if(funcoes)
    {
        return JSON.parse(funcoes);
    }else{
        return [];
    }
}

function destroyUsuario(id, name){
    $.confirm({
        title: 'Excluir Usuário!',
        content: 'Deseja realmente excluir a Usuário <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/usuario/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){
	 $("#content-modal-usuario").on("click", "#fechar-usuario", function() {
        $('#modal-form-usuario').modal('toggle');
    });

    $("#content-modal-usuario").on("click", "#add-funcao", function() 
    {   
        var idFuncao   = $("#select_funcao").val();
        var textFuncao = $("#select_funcao :selected").text();
        var funcoes    = getFuncao();
        var adicionar  = true; 

        if(idFuncao > 0)
        {
            funcoes.forEach(function(item, index) {
                if(item.id == idFuncao)
                {
                    notifyAlert('warning', "Função já existente na lista.");
                    adicionar = false;
                }
            });

            if(adicionar == true)
            {
                addFuncao(funcoes, idFuncao, textFuncao, false);
            }
            $("#select_funcao").val(0).change();
        }
        else{
            notifyAlert('warning', "Selecione uma Função!");
        }

    });


    $('#content-modal-usuario').on('submit', '#form-usuario', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-usuario").disabled = true;

        var id 				=  $('#id').val(); 
        var name 			=  $('#name').val();
        var email 			=  $('#email').val();
        var password 		=  $('#password').val(); 
        var repeat_password =  $('#repeat-password').val(); 
        var funcoes 		= $('#funcoes-values').val();
        

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/usuario/store",
            data: {_token: _token, id: id, name: name ,email: email , password: password, repeat_password: repeat_password, funcoes: funcoes},
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){ 
                        $('#modal-form-usuario').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-usuario").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});