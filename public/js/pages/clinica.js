$('#cnpj').inputmask({'mask': "99.999.999/9999-99", greedy: false, reverse: true, autoUnmask: true});
$('#cep').inputmask({'mask': "99999-999", greedy: false, reverse: true, autoUnmask: true});
$('#telefone').inputmask({'mask': "(99) 9999-9999", greedy: false, reverse: true, autoUnmask: true});
$('#celular').inputmask({'mask': "(99) 99999-9999", greedy: false, reverse: true, autoUnmask: true});

$(document).ready(function(){

    $("#fechar-clinica").on("click", function() {
        window.location.href = urlBase + "panel/sistema";
    });

    $('#form-clinica').on('submit', function(e){        
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-clinica").disabled = true;
        var formData = new FormData(this);
        
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/clinica/store",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(result){
                if (result.success === true) {
                    $('html,body').animate({scrollTop: 0},'slow');
                    notify('success', result.msg);
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }else{
                    $('html,body').animate({scrollTop: 0},'slow');
                    document.getElementById("salvar-clinica").disabled = false;
                    notify('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
})