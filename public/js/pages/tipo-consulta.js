//functions
function createTipoConsulta(){
    $.get(urlBase + "panel/tipo-consulta/create", function(response){
        $("#content-modal-tipo-consulta").html(response);
        $('#modal-form-tipo-consulta').modal('show');
    });
}

function editTipoConsulta(id){
    $.get(urlBase + "panel/tipo-consulta/edit/" + id, function(response){
        $("#content-modal-tipo-consulta").html(response);
        $('#modal-form-tipo-consulta').modal('show');
    });
}

function destroyTipoConsulta(id, name){
    $.confirm({
        title: 'Excluir Tipo de Consulta!',
        content: 'Deseja realmente excluir o Tipo de Consulta <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/tipo-consulta/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){
    $("#content-modal-tipo-consulta").on("click", "#fechar-tipo-consulta", function() {
        $('#modal-form-tipo-consulta').modal('toggle');
    });

    $('#content-modal-tipo-consulta').on('submit', '#form-tipo-consulta', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-tipo-consulta").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/tipo-consulta/store",
            data: $("#form-tipo-consulta").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){ 
                        $('#modal-form-tipo-consulta').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-tipo-consulta").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});
