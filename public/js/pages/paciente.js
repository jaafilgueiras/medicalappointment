$('#cpf').inputmask({'mask': "999.999.999-99", greedy: false, reverse: true, autoUnmask: true});
$('#telefone').inputmask({'mask': "(99) 99999-9999", greedy: false, reverse: true, autoUnmask: true});
$('#data-nascimento').inputmask({'mask': "99/99/9999", greedy: false, reverse: true, autoUnmask: true});
$('#cep').inputmask({'mask': "99999-999", greedy: false, reverse: true, autoUnmask: true});

function createPaciente(){
    window.location.href = urlBase + "panel/paciente/create";
}

function editPaciente(id){
    window.location.href = urlBase + "panel/paciente/edit/" + id;
}

function destroyPaciente(id, name){
    $.confirm({
        title: 'Excluir Paciente!',
        content: 'Deseja realmente excluir Paciente <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/paciente/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }

                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){

    $("#fechar-paciente").on("click", function() {
        window.location.href = urlBase + "panel/paciente";
    });

    $('#form-paciente').on('submit', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-paciente").disabled = true;
        var formData = new FormData(this);

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/paciente/store",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(result){
                if (result.success === true) {
                    $('html,body').animate({scrollTop: 0},'slow');
                    notify('success', result.msg);
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }else{
                    $('html,body').animate({scrollTop: 0},'slow');
                    document.getElementById("salvar-paciente").disabled = false;
                    notify('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
})