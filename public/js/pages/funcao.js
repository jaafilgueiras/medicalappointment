//functions
function createFuncao(){
    $.get(urlBase + "panel/funcao/create", function(response){
        $("#content-modal-funcao").html(response);
        $('#modal-form-funcao').modal('show');        
    });
}

function editFuncao(id){
    $.get(urlBase + "panel/funcao/edit/" + id, function(response){
        $("#content-modal-funcao").html(response);
        $('#modal-form-funcao').modal('show');
    });
}
//carregar as permissões
function loadPermissions()
{
    var id = $("#id").val();

    if(id > 0 || id != '')
    {
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});

        $.ajax({
            type: "GET",
            url: urlBase + "panel/funcao/get-permissions/" + id,
            data: {},
            success: function(data){
                if(data)
                {
                    console.log(data);
                    data.forEach(function(item, index) {
                        addPermission(getPermissions(), item.permission_id,  item.description, false);
                    });
                }
                
                waitingDialog.hide();
            }, error: function(){
                notifyAlert('danger', 'Erro ao carregar Permissões.');
                waitingDialog.hide();
            }
        });
    }
}
//set valor na table as permissões
function setPermissions(permissions)
{
    $("#permissions-values").val(JSON.stringify(permissions));
}
//adicionar permissão
function addPermission(array, idPermission, namePermission, deletar)
{
    array.push({ id: idPermission, name: namePermission , delete : deletar});
    setPermissions(array);
    console.log(array);
    $("#table-body-permissoes").append('<tr><td>'+ namePermission +'</td><td style="width: 5%;"><center><button type="button" class="btn btn-danger btn-flat btn-xs" onclick="removePermission(this, '+ idPermission +');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></center></td></tr>');
}
//remover permissão
function removePermission(element, id)
{
    var permissoes = getPermissions();

    permissoes.forEach(function(item, index, object) {
        if(item.id == id)
        {
            item.delete = true;
            object.splice(index, 1);
            
            $(element).closest("tr").remove();
        }
    });
    console.log(permissoes);
    setPermissions(permissoes);
}

function getPermissions()
{
    var permissions = $("#permissions-values").val();
    if(permissions)
    {
        return JSON.parse(permissions);
    }else{
        return [];
    }
}


function destroyFuncao(id, name){
    $.confirm({
        title: 'Excluir Função!',
        content: 'Deseja realmente excluir a Função <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/funcao/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}
//jquery

$(document).ready(function(){
    $("#content-modal-funcao").on("click", "#fechar-funcao", function() {
        $('#modal-form-funcao').modal('toggle');
    });

    $("#content-modal-funcao").on("click", "#add-permissao", function() 
    {   
        var idPermissao   = $("#select_permission").val();
        var textPermissao = $("#select_permission :selected").text();
        var permissoes    = getPermissions();
        var adicionar     = true; 

        if(idPermissao > 0)
        {
            permissoes.forEach(function(item, index) {
                if(item.id == idPermissao)
                {
                    notifyAlert('warning', "Permissão já existente na lista.");
                    adicionar = false;
                }
            });

            if(adicionar == true)
            {
                addPermission(permissoes, idPermissao, textPermissao, false);
            }
            $("#select_permission").val(0).change();
        }
        else{
            notifyAlert('warning', "Selecione uma Permissão!");
        }

    });

    $('#content-modal-funcao').on('submit', '#form-funcao', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-funcao").disabled = true;

        var id =  $('#id').val(); 
        var name =  $('#name').val();
        var description =  $('#description').val(); 
        var permissions = $('#permissions-values').val();
        

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/funcao/store",
            data: {_token: _token, id: id, name: name ,description: description , permissoes: permissions},
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){ 
                        $('#modal-form-funcao').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-funcao").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});
