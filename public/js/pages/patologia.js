//functions
function createPatologia(){
    window.location.href = urlBase + "panel/patologia/create";
}

function editPatologia(id){
    window.location.href = urlBase + "panel/patologia/edit/"+ id;
}

function showPatologia(id){
    window.location.href = urlBase + "panel/patologia/show/"+ id;
}

function loadPatologias(){
	var id = $("#id").val();

    if(id > 0 || id != '')
    {
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});

        $.ajax({
            type: "GET",
            url: urlBase + "panel/patologia/get-patologias/" + id,
            data: {},
            success: function(data){
                if(data)
                {
                    data.forEach(function(item, index) {
                        addPatologia(getPatologia(), item.sintoma_id, item.name_sintoma,  item.consequencia_id, item.name_consequencia, false);
                    });
                }
                
                waitingDialog.hide();
            }, error: function(){
                notify('danger', 'Erro ao carregar Patologias.');
                waitingDialog.hide();
            }
        });
    }
}

function addPatologia(array, idSintoma, nameSintoma, idConsequencia, nameConsequencia, deletar)
{
    array.push({ sintoma_id: idSintoma, name_sintoma: nameSintoma,  consequencia_id: idConsequencia , name_consequencia: nameConsequencia,  delete : deletar});
    setPatologia(array);
    $("#table-body-patologias").append('<tr><td>'+ nameSintoma +'</td><td>'+ nameConsequencia +'</td><td style="width: 5%;"><center><button type="button" class="btn btn-danger btn-flat btn-xs" onclick="removePatologia(this, '+ idSintoma +','+ idConsequencia +');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></center></td></tr>');
}

function getPatologia()
{
    var patologia = $("#patologias-values").val();
    if(patologia)
    {
        return JSON.parse(patologia);
    }else{
        return [];
    }
}

function setPatologia(patologias)
{
    $("#patologias-values").val(JSON.stringify(patologias));
}

function removePatologia(element, idSintoma, idConsequencia)
{
    var patologias = getPatologia();
  
    patologias.forEach(function(item, index, object) {
        if(item.sintoma_id == idSintoma)
        {
            item.delete = true;
            object.splice(index, 1);
            
            $(element).closest("tr").remove();
        }
    });
    setPatologia(patologias);
}

function destroyPatologia(id, name){
    $.confirm({
        title: 'Excluir Patologia!',
        content: 'Deseja realmente excluir a Patologia <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/patologia/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}
$(document).ready(function(){

	$("#add-patologias").on("click", function(){ 
        var idSintoma          = $("#select_sintomas").val();
        var textSintoma   	   = $("#select_sintomas :selected").text();
        var idConsequencia     = $("#select_consequencias").val();
        var textConseqeuncia   = $("#select_consequencias :selected").text();
        var patologias    	   = getPatologia();
        var adicionar     	   = true; 

        if(idSintoma > 0 && idConsequencia > 0)
        {
            patologias.forEach(function(item, index) {
                if(item.sintoma_id == idSintoma && item.consequencia_id == idConsequencia)
                {
                    notify('warning', "Patologia já existente na lista.");
                    adicionar = false;
                }
            });

            if(adicionar == true)
            {
            	addPatologia(patologias, idSintoma, textSintoma,  idConsequencia, textConseqeuncia, false);

            }
            $("#select_sintomas").val(0).change();
            $("#select_consequencias").val(0).change();
        }
        else{
        	if(idSintoma == 0){
            	notify('warning', "Selecione uma Sintoma!");
            	return;
        	}
        	if(idConsequencia == 0){
        		notify('warning', "Selecione uma Consequência!");
        		return;
        	}
        }

    });


    $("#fechar-patologia").on("click", function() {
        window.location.href = urlBase + "panel/patologia";
    });

    $('#form-patologia').on('submit', function(e){        
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-patologia").disabled = true;
        var id 				  =  $("#id").val();
        var nome 			  =	 $("#nome").val();	
        var nome_cinentifico  =	 $("#nome_cientifico").val();
        var origem  		  =	 $("#origem").val();
        var descricao		  =	 $("#descricao").val();
        var patologias_values =	 $("#patologias-values").val(); 
        
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/patologia/store",
            data: {_token: _token, id: id, nome: nome, nome_cientifico: nome_cinentifico, origem: origem,descricao: descricao, patologias: patologias_values},
            success: function(result){
                if (result.success === true) {
                    $('html,body').animate({scrollTop: 0},'slow');
                    notify('success', result.msg);
                    setTimeout(function(){
                        window.location.href = urlBase + "panel/patologia";
                    }, 1000);
                }else{
                    $('html,body').animate({scrollTop: 0},'slow');
                    document.getElementById("salvar-patologia").disabled = false;
                    notify('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});