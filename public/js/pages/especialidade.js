//functions
function createEspecialidade(){
    $.get(urlBase + "panel/especialidade/create", function(response){
        $("#content-modal-especialidade").html(response);
        $('#modal-form-especialidade').modal('show');
    });
}

function editEspecialidade(id){
    $.get(urlBase + "panel/especialidade/edit/" + id, function(response){
        $("#content-modal-especialidade").html(response);
        $('#modal-form-especialidade').modal('show');
    });
}

function destroyEspecialidade(id, name){
    $.confirm({
        title: 'Excluir Especialidade!',
        content: 'Deseja realmente excluir a Especialidade <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/especialidade/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){
    $("#content-modal-especialidade").on("click", "#fechar-especialidade", function() {
        $('#modal-form-especialidade').modal('toggle');
    });

    $('#content-modal-especialidade').on('submit', '#form-especialidade', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-especialidade").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/especialidade/store",
            data: $("#form-especialidade").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){ 
                        $('#modal-form-especialidade').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-especialidade").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});
