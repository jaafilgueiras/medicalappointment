//functions
function createConsequencia(){
    $.get(urlBase + "panel/consequencia/create", function(response){
        $("#content-modal-consequencia").html(response);
        $('#modal-form-consequencia').modal('show');        
    });
}

function editConsequencia(id){
    $.get(urlBase + "panel/consequencia/edit/" + id, function(response){
        $("#content-modal-consequencia").html(response);
        $('#modal-form-consequencia').modal('show');
    });
}

function destroyConsequencia(id, name){
    $.confirm({
        title: 'Excluir Consequencia!',
        content: 'Deseja realmente excluir a Consequencia <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "panel/consequencia/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){
    $("#content-modal-consequencia").on("click", "#fechar-consequencia", function() {
        $('#modal-form-consequencia').modal('toggle');
    });

    $('#content-modal-consequencia').on('submit', '#form-consequencia', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-consequencia").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "panel/consequencia/store",
            data: $("#form-consequencia").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){ 
                        $('#modal-form-consequencia').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-consequencia").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});
