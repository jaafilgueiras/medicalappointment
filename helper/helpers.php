<?php

function arrayValidator($arr) {
        
    $erros = '<ul>';
            
    foreach ($arr->toArray() as $erro)
    {
        foreach ($erro as $msg)
        {
            $erros .= '<li>' .$msg. '</li>';
        }
    }
        
    $erros .= '</ul>';
        
    return $erros;
}

function toSelect( array $object, $key, $value )
{
    if(count($object) > 0)
    {
        $data = array();
    
        $data[0] = 'Selecione';
        foreach ($object as $row) {
            $data[$row[$key]] = $row[$value];
        }

        return $data;
    }else{
        $data = array();

        $data[0] = 'Selecione';
        
        return $data;
    }
}

function dateToSave($data,$hour = null){
    if(!isset($data)){
        return null;
    }

    $formatDate = str_replace("/", ".", $data);

    if($hour){
        return date('Y-m-d H:i', strtotime($formatDate));
    }

    return date('Y-m-d', strtotime($formatDate));
}

function dateToView($value, $hour = null) {
    if(!isset($value)){
        return '';
    }

    if($hour){
        return date('d/m/Y H:i:s', strtotime($value));
    }

    return date('d/m/Y', strtotime($value));
}

function telefoneToView($numero, $onlyNumber = false) {
    if($onlyNumber == true){
        return substr($numero,0,4)."-".substr($numero,4,7);
    }
    return "(".substr($numero,0,2).") ".substr($numero,2,5)."-".substr($numero,7,4);
}