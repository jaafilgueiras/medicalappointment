<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => 'panel'], function () {

    Route::get('/', 'PanelController@index');
    Route::get('/sistema', 'PanelController@sistema');

    include('routes/clinica.php');
    include('routes/funcao.php');
    include('routes/permissao.php');
    include('routes/usuario.php');
    include('routes/sintoma.php');
    include('routes/consequencia.php');
    include('routes/patologia.php');
    include('routes/especialidade.php');
    include('routes/tipo-consulta.php');
    include('routes/paciente.php');
});