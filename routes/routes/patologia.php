<?php 

    Route::group(['prefix' => 'patologia'], function () {

        Route::get('/', 'PatologiaController@index');

        Route::get('/create', 'PatologiaController@create');

        Route::post('/store', 'PatologiaController@store');

        Route::get('/edit/{patologia}', 'PatologiaController@edit');

        Route::post('/destroy', 'PatologiaController@destroy');

        Route::get('/get-patologias/{idPatologia}', 'PatologiaController@getPatologias');

        Route::get('/show/{id}', 'PatologiaController@show');
        
    });
