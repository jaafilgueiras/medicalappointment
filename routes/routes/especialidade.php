<?php 

    Route::group(['prefix' => 'especialidade'], function () {

        Route::get('/', 'EspecialidadeController@index');

        Route::get('/create', 'EspecialidadeController@create');

        Route::post('/store', 'EspecialidadeController@store');

        Route::get('/edit/{especialidade}', 'EspecialidadeController@edit');

        Route::post('/destroy', 'EspecialidadeController@destroy');

    });
