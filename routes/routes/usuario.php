<?php 

    Route::group(['prefix' => 'usuario'], function () {

        Route::get('/', 'UserController@index');

        Route::get('/create', 'UserController@create');

        Route::post('/store', 'UserController@store');

        Route::get('/edit/{usuario}', 'UserController@edit');

        Route::post('/destroy', 'UserController@destroy');

        Route::get('/get-funcoes/{idFuncao}', 'UserController@getFuncoes');

    });
