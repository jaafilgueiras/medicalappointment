<?php 

    Route::group(['prefix' => 'tipo-consulta'], function () {

        Route::get('/', 'TipoConsultaController@index');

        Route::get('/create', 'TipoConsultaController@create');

        Route::post('/store', 'TipoConsultaController@store');

        Route::get('/edit/{tipoConsulta}', 'TipoConsultaController@edit');

        Route::post('/destroy', 'TipoConsultaController@destroy');

    });
