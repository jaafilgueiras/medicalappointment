<?php

Route::group(['prefix' => 'paciente'], function () {

    Route::get('/', 'PacienteController@index');

    Route::get('/create', 'PacienteController@create');

    Route::post('/store', 'PacienteController@store');

    Route::get('/edit/{paciente}', 'PacienteController@edit');

    Route::post('/destroy', 'PacienteController@destroy');

});
