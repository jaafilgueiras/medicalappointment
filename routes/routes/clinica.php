<?php 

    Route::group(['prefix' => 'clinica'], function () {

        Route::get('/', 'ClinicaController@index');

        Route::get('/create', 'ClinicaController@create');

        Route::post('/store', 'ClinicaController@store');

        Route::get('/edit/{clinica}', 'ClinicaController@edit');

        Route::post('/destroy', 'ClinicaController@destroy');

    });
