<?php 

    Route::group(['prefix' => 'sintoma'], function () {

        Route::get('/', 'SintomaController@index');

        Route::get('/create', 'SintomaController@create');

        Route::post('/store', 'SintomaController@store');

        Route::get('/edit/{sintoma}', 'SintomaController@edit');

        Route::post('/destroy', 'SintomaController@destroy');

    });
