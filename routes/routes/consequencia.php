<?php 

    Route::group(['prefix' => 'consequencia'], function () {

        Route::get('/', 'ConsequenciaController@index');

        Route::get('/create', 'ConsequenciaController@create');

        Route::post('/store', 'ConsequenciaController@store');

        Route::get('/edit/{consequencia}', 'ConsequenciaController@edit');

        Route::post('/destroy', 'ConsequenciaController@destroy');

    });
