<?php 

    Route::group(['prefix' => 'funcao'], function () {

        Route::get('/', 'FuncaoController@index');

        Route::get('/create', 'FuncaoController@create');

        Route::post('/store', 'FuncaoController@store');

        Route::get('/edit/{funcao}', 'FuncaoController@edit');

        Route::post('/destroy', 'FuncaoController@destroy');

        Route::get('/get-permissions/{idRole}', 'FuncaoController@getPermissions');

    });
